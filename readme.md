## Github Repos

This app uses Laravel Socialite to handle Github oAuth with a VueJS SPA frontend.

The SPA source code can be found in the `resources/spa` directory

To start the web-server copy the supplied .env and run;

```bash
docker-compose up
```

The deployment contains a self-signed SSL certificate whch you may need to enable.


### What would I change?

Although I feel this follow the brief, it is a little rough around the edges. Things that could improve this app include;

1. *SPA as a Module* - Have the SPA in a separate repo and load it as a module.
2. *Env's to be stored in a secret manager* - Hashicorps, AWS, etc.
2. *API Calls* - I decided to go with checking the branches, however this might not be correct and maybe `refs/heads` is better. Would need to run some tests on real life scenarios.
2. *Error Handling* - Only contains basic server error handing; the router could check for more specific errors
3. *Transitions* - The pages simply follow each other. There is a global loader but it would be nice if it used Vue's `<transition>` properties.
4. *Local storage* - The app logs a user out once the browser is closed. As the github repo does not use JWT or a refresh token I decided this would suffice. Checking the Rate-Limit-Header would be important in a real-life situation.
5. *Responsiveness* - Only built for Chrome and would need browser testing.
