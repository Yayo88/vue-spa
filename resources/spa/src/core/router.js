import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store'

Vue.use(VueRouter);


const router = new VueRouter({
    routes: [
        {
            path: '/',
            component: require('./../components/Login.vue').default,
        },
        {
            path: '/dashboard',
            component: require('./../components/Dashboard.vue').default,
            meta: {
                authMiddleware: true
            },
        },{
            path: '/repository/:username/:repositoryName',
            component: require('./../components/Repository.vue').default,
            meta: {
                authMiddleware: true
            },
        },
        {
            path: '*',
            component: require('./../components/404.vue').default,
            meta: {
                title: "404: Page Not Found"
            }
        }
    ]
});


router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.authMiddleware)) {
        if (store.state.accessToken == null) {
            next({
                path: '/',
                params: {
                    nextUrl: to.fullPath
                }
            })
        } else {
            next()
        }
    } else {
        next()
    }
});


router.beforeEach((to, from, next) => {
    store.state.globalLoader = true;
    next()
});
router.afterEach((to, from) => {
    store.state.globalLoader = false;
});

export default router;
