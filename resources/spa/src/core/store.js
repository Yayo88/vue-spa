import Vue from 'vue'
import Vuex from 'vuex'
import VueSweetalert2 from 'vue-sweetalert2'


import api from './api'

Vue.use(Vuex);
Vue.use(VueSweetalert2, {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674'
});

import * as loader from './../assets/lottie/loader.json';

const state = {
    accessToken: null,
    nickname: null,
    avatar: null,
    name: null,
    isAuthenticated: false,
    repos: [],
    repository: null,
    globalLoader: false,
    loader: loader
};

const getters = {};

const actions = {
    // eslint-disable-next-line
    getRepository({state}, object){
        return api.get({
            resource: "repos",
            user: object.user,
            repository: object.repository
        })
    },
    // eslint-disable-next-line
    getRepositories({state}, object){
        return api.get({
            resource: "users",
            user: object.user,
            action: "repos",
        })
    },
    // eslint-disable-next-line
    getRepositoryBranches({state}, object){
        return api.get({
            resource: "repos",
            user: object.user,
            repository: object.repository,
            action: "branches",
        })
    },
    // eslint-disable-next-line
    getCommits({state}, object){
        return api.get({
            resource: "repos",
            user: object.user,
            repository: object.repository,
            action: "commits",
        })
    },


    destroySession({state}){

        state.accessToken = null;
        state.nickname = null;
        state.avatar = null;
        state.name = null;
        state.isAuthenticated = false;
        state.repos = [];
        state.repository = null;

        return true;

    },

    handleRouteError({state}, response){
        state.globalLoader = false;

        if (response.hasOwnProperty('status')) {

            let message = response.body.hasOwnProperty('message') ? response.body.message : 'Sorry, something went wrong';

            if (response.status === 404) {
                return Vue.swal({
                    title: 'Sorry we can\'t find that!',
                    text: message,
                    type: 'danger'
                });

            } else if (response.status === 403) {

                return Vue.swal({
                    title: 'Sorry, you don\'t have access to that resource!',
                    text: message,
                    type: 'error'
                });

            } else {

                return Vue.swal({
                    title: 'Sorry, something went wrong.',
                    text: 'Something is wrong',
                    type: 'danger'
                });

            }


        }


        return Vue.swal({
            title: 'Sorry, something went wrong.',
            text: 'Something is wrong',
            type: 'danger'
        });

    }

};

export default new Vuex.Store({
    state,
    getters,
    actions
});
