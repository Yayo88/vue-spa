import {
    GithubResource
} from './resources';

export default {
    get(obj) {
        return GithubResource.get(obj);
    }
};
