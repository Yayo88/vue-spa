import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router';

import store from "./../store.js"

Vue.use(VueResource);
Vue.use(VueRouter);

const API_ENDPOINT = 'https://api.github.com';

Vue.http.interceptors.push((request, next)=> {
    request.headers = request.headers || {};
    request.headers.set('Content-Type', 'application/json');

    if(request.hasOwnProperty('params')){
       request.params['access_token'] = store.state.accessToken;
    }

    next();
});

export const GithubResource = Vue.resource(API_ENDPOINT + '/{resource}/{user}{/repository}{/action}');
