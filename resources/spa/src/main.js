import Vue from 'vue'

import App from './App.vue'

import router from './core/router'
import store from './core/store'

import Sidebar from './components/incs/Sidebar.vue';
import Loader from './components/incs/Loader.vue';

Vue.component('sidebar', Sidebar);
Vue.component('loader', Loader);


Vue.config.productionTip = false;

new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app');
