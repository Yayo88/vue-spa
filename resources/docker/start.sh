#!/bin/bash
set -e

echo ">> Installing Composer"

cd /var/www/html && composer install

echo ">> Staring supervisord"

/usr/bin/supervisord -n -c /etc/supervisord.conf

echo ">> Staring php-fpm"

php-fpm
