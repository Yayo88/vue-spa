<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Github Repositories</title>
    <link href=" {{ asset('css/app.css') }}" rel="stylesheet">
    <link href=" {{ asset('css/chunk-vendors.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <app></app>
</div>
<script src="{{ asset('js/chunk-vendors.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
